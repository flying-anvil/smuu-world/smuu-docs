# Palettes

A palette is a collection of colors.

Each palette has 16 slots, which means they can hold up to 16 colors.
Maybe there will be small/short palettes with only 8 slots (to prevent the "too much choice" symptom).

Each color is a full RGBA8 color, meaning 4 color channels, 8-bit / 1 byte each.

## Storing

Certain tools that deal with retro graphics like YY-CHR already have a file format for RGB (without alpha) palettes.
It's a binary format where each byte represents one color channel.
This means for an RGB color with a bit-depth of 8-bit, 3 bytes are one color.

See <https://flying-anvil.gitlab.io/smw-hacking-guide/documentation/files/palette.html>
for a more in-depth documentation (although it's super simple).

This format could be supported as well and even adopted to support RGBA colors.

When using those .pal files, the alpha channel could be set to fully opaque, except for the very first color,
which is usually fully transparent. However, existing files might contain more than one palette, or an incomplete
one (from the perspective of Smuu-World).

To optionally support palettes with alpha, the same format could be used but with 4 bytes per color.
The suggested file extension would be `.pala` or `.rgba` (_TBD_).

## Standardization

To make palettes easily interchangeable between blocks and sprites, a standard layout is needed.
As there are multiple ways a palette could be layed out, multiple standards seem plausible.

In general, the following categories of color seem logical:

- Transparent
- Outline
- Highlight
- Accent
- Primary
- Secondary
- Tertiary
- Quaternary (maybe, maybe not)

### Short palettes examples

#### Short 1

Transparency, outline, highlight, 3 primary, 2 secondary

![Short 1](./img/short-1.png)
