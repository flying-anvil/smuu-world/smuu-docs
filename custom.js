// Adds classes to those tiny inline block icons to make them not stick to the top of the line.
const addInlineImageClass = () => {
    const images = document.querySelectorAll('img[src*=blocks]');
    for (const img of images) {
        img.classList.add('inline');
    }
}

const addImageTitles = () => {
    // Selects all images with a non-empty alt attribute that also don't have a title
    const imagesWithAlt = document.querySelectorAll('img[alt]:not(img[alt=""]):not(img[title])');
    for (const img of imagesWithAlt) {
        img.setAttribute('title', img.getAttribute('alt'))
    }
}

addInlineImageClass();
addImageTitles();
