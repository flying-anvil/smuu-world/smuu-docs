#!/usr/bin/env just --justfile

repoUrl := "https://gitlab.com/flying-anvil/smuu-world/smuu-docs"

start:
  mdbook serve --hostname 0.0.0.0

mr:
  echo "{{repoUrl}}/-/merge_requests/new?merge_request%5Bsource_branch%5D=$(git branch | grep \* | cut -d " " -f2)"
  xdg-open "{{repoUrl}}/-/merge_requests/new?merge_request%5Bsource_branch%5D=$(git branch | grep \* | cut -d " " -f2)" > /dev/null 2>&1 &
